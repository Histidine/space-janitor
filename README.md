# README #

Space Janitor is an utility mod for the game [Starsector](http://fractalsoftworks.com). It fixes the non-expiring debris field and non-despawning scavenger fleet bugs.

Current release version: v1.0

### Setup instructions ###
Check out the repo to Starsector/mods/Space Janitor (or some other folder name) and it can be played immediately. 

Create a project with Space Janitor/jars/src as a source folder to compile the Java files.

### License ###
The `cleanup` console command code is adapted from the [Outer Rim Alliance mod](http://fractalsoftworks.com/forum/index.php?topic=11646.0) by Tartiflette and licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license](https://creativecommons.org/licenses/by-nc-sa/4.0/).

All other code is licensed under the [MIT License (Expat License version)](https://opensource.org/licenses/MIT) except where otherwise specified.

All other assets are released as [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) unless otherwise specified.
