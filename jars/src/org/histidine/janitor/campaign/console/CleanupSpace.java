package org.histidine.janitor.campaign.console;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignClockAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.lazywizard.console.BaseCommand;
import org.lazywizard.console.CommonStrings;
import org.lazywizard.console.Console;

/**
 * Prints number of extant debris fields and derelict hulls; optionally cleans them up
 * Uses Tartiflette code
 */
public class CleanupSpace implements BaseCommand {
        
    public static Logger log = Global.getLogger(CleanupSpace.class);

    @Override
    public CommandResult runCommand(String args, CommandContext context) {
        if (context != CommandContext.CAMPAIGN_MAP) {
            Console.showMessage(CommonStrings.ERROR_CAMPAIGN_ONLY);
            return CommandResult.WRONG_CONTEXT;
        }        
        
		boolean clearDebris = false;
		boolean clearDerelicts = false;
        if (!args.isEmpty())
        {
            String[] tmp = args.split(" ");
			if (tmp[0].toLowerCase().equals("debris"))
				clearDebris = true;
			if (tmp[0].toLowerCase().startsWith("derelict"))
				clearDerelicts = true;
        }
        
        CampaignClockAPI clock = Global.getSector().getClock();
        Console.showMessage("Date string: " + clock.getDateString());
        Console.showMessage(" ");
		
		SectorAPI sector = Global.getSector();
        
        Console.showMessage("_______");
        Console.showMessage("DEBRIS");
        Console.showMessage("_______");
        Console.showMessage(" ");
		
		// FIXME: once an entity is discovered it becomes non-discoverable!!
        List<SectorEntityToken> allDebris = sector.getEntitiesWithTag("debris");
        List<SectorEntityToken> allWrecks = sector.getEntitiesWithTag("salvageable");
		List<SectorEntityToken> debrisFromBattle = new ArrayList<>();
		List<SectorEntityToken> wrecksFromBattle = new ArrayList<>();
		for (SectorEntityToken d : allDebris){
			if(!d.isDiscoverable()){
				debrisFromBattle.add(d);	
			}
		}
		for (SectorEntityToken d : allWrecks){
			if(!d.isDiscoverable()){
				wrecksFromBattle.add(d);
				Console.showMessage("Entity " + d.getFullName() + " is non-discoverable");
			}
		}
        
        Console.showMessage("   Debris fields: " + allDebris.size()
				+ " (" + debrisFromBattle.size() + " cleanable)");     
        Console.showMessage("   Derelicts: " + allWrecks.size()
				+ " (" + wrecksFromBattle.size() + " cleanable)");  
        Console.showMessage("       Total: " + (allDebris.size() + allWrecks.size()) 
				+ " (" + (debrisFromBattle.size() + wrecksFromBattle.size())  +" cleanable)");     
        Console.showMessage(" ");
        
        if (clearDebris) {
            for (SectorEntityToken d : debrisFromBattle){
                d.setExpired(true);
            }
			Console.showMessage("   Cleaned up " + debrisFromBattle.size() + " debris fields");
        }
		if (clearDerelicts) {
			for (SectorEntityToken d : wrecksFromBattle){
                d.setExpired(true);
            }
			Console.showMessage("   Cleaned up " + wrecksFromBattle.size() + " derelicts");
        }
        
        return CommandResult.SUCCESS;
    }
}
