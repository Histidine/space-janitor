package org.histidine.janitor.campaign;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;

public class JanitorModPlugin extends BaseModPlugin {
	
	public boolean isNexerelin = false;
	
	@Override
    public void onGameLoad(boolean newGame) {
		if (!isNexerelin)
			Global.getSector().addTransientListener(new ScavengerCleaner());
    }
	
	@Override
	public void onApplicationLoad() throws Exception {
		isNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
	}
}